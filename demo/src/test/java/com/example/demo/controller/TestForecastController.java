package com.example.demo.controller;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.demo.exception.ForecastNotFoundException;
import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.StandardReleaseResponse;
import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.service.IForecastService;

@SpringBootTest
public class TestForecastController {

	@InjectMocks
	private ForecastController forecastcontroller;

	@Mock
	private IForecastService iforecastservice;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all forecast details is given it should return an iterable of type forecast")
	public void WhenGetAllForecast_ThenReturnIterable() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",  7, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public",  7, "Manoj", "Manoj", newaccount, role);
		ArrayList<Forecast> list = new ArrayList<Forecast>();
		list.add(newforecast);
		list.add(newforecast2);
		Mockito.when(iforecastservice.getAllForecasts()).thenReturn(list);
//		Iterable<Forecast> iterable = iforecastrepository.findAll();
		Iterable<Forecast> expected = forecastcontroller.getAllForecasts();
		Assertions.assertEquals(expected, list);
	}

	@Test
	@DisplayName("When get forecast by role is given it should return the forecast list")
	public void WhenGetForecastsForRole_ThenReturnIterable() throws StandardReleaseResponse {
		// Positive case

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public",  7, "Manoj", "Manoj", newaccount, role);

		ArrayList<Forecast> list = new ArrayList<Forecast>();
		list.add(newforecast);
		list.add(newforecast2);
		Mockito.when(iforecastservice.findForecastsForRole(role.getRoleID(), "public")).thenReturn(list);

		Iterable<Forecast> expected = forecastcontroller.findForecastsForRole(role.getRoleID(), "public");
		Assertions.assertEquals(expected, list);
	}

	@Test
	@DisplayName("When save forecast is given it should return the saved forecast")
	public void WhenSaveNewForecast_ThenSave() {
		// Positive case
		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);

		Mockito.when(iforecastservice.createNewForecast(newforecast)).thenReturn(newforecast);

		Forecast expected = forecastcontroller.createNewforecast(newforecast);

		Assertions.assertEquals(expected, newforecast);

		Mockito.verify(iforecastservice).createNewForecast(newforecast);
	}

	@Test
	@DisplayName("When delete forecast is given it should perform delete forecast")
	public void WhenDeleteForecast_ThenDelete() {

		Map<String, Boolean> value = new HashMap<String, Boolean>();
		value.put("Deleted", true);
//		Mockito.doNothing().doThrow(new ForecastNotFoundException()).when(iforecastservice).deleteForecast(new String(), new String());
		Mockito.when(iforecastservice.deleteForecast("public", "new20")).thenReturn(value);
		Map<String, Boolean> expected = forecastcontroller.deleteForecast("public", "new20");
//		Map<String, Boolean> expected = ;
//		Map<String, Boolean> expected2 = ;
		Assertions.assertEquals(value, expected);
		Mockito.verify(iforecastservice).deleteForecast("public", "new20");
//		Assertions.assertEquals(value, forecastcontroller.deleteForecast("public", "New12"));
	}

	@Test
	@DisplayName("When get forecast by id is given it should return the forecast")
	public void WhenGetForecastById_ThenReturnObject() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);

		Mockito.when(iforecastservice.getForecastByID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9")))
				.thenReturn(newforecast);
		Mockito.when(iforecastservice.getForecastByID(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057")))
				.thenReturn(newforecast2);

		ResponseEntity<Forecast> expected = forecastcontroller
				.getForecastID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"));
		ResponseEntity<Forecast> expected2 = forecastcontroller
				.getForecastID(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"));

		Assertions.assertEquals(expected.getBody(), newforecast);
		Assertions.assertTrue(expected2.getBody().toString().equals(newforecast2.toString()));
	}

	@Test
	@DisplayName("When invalid forecast id is given it should not return the forecast")
	public void WhenGetForecastByIdNotPresent_ThenReturnObject() {
		Assertions.assertThrows(ForecastNotFoundException.class, () -> {
			forecastcontroller.getForecastID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"));
		});
	}

	@Test
	@DisplayName("When update forecast by id is given it should return the updated forecast")
	public void WhenUpdateForecastById_ThenReturnUpdatedForecast() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast forecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public", 7, "Manoj", "Manoj", newaccount, role);
		ArrayList<String> list = new ArrayList<String>();
		list.add("New20");
		list.add("New21");
		Mockito.when(iforecastservice.updateForecastDetails(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"),
				forecast)).thenReturn(forecast);
//		Mockito.when(iforecastservice.findById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"))).thenReturn(forecast);
//		Mockito.when(iforecastservice.save(newforecast2)).thenReturn(newforecast2);

		ResponseEntity<Forecast> expected = forecastcontroller
				.updateForecastDetails(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), forecast);
		Assertions.assertEquals(expected.getBody(), forecast);
//		Mockito.when(iforecastrepository.save(newforecast2)).thenReturn(newforecast2);
//		Mockito.verify(iforecastrepository).save(newforecast2);
	}
}
