
package com.example.demo.testserviceimpl;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IAccountRepository;
import com.example.demo.repository.ICategoryRepository;
import com.example.demo.repository.IForecastRepository;
import com.example.demo.repository.IUserDetailsRepository;
import com.example.demo.repository.IUserRoleRepository;
import com.example.demo.serviceimpl.CategoryServiceImpl;

@SpringBootTest
public class TestCategoryServiceImpl {

	@InjectMocks
	private CategoryServiceImpl categoryserviceimpl;

	@Mock
	private ICategoryRepository icategoryrepository;

	@Mock
	private IForecastRepository iforecastrepository;

	@Mock
	private IAccountRepository iaccountrepository;

	@Mock
	private IUserDetailsRepository iuserdetailsrepository;

	@Mock
	private IUserRoleRepository iuserrolerepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all categories details is given it should return an iterable of type category")
	public void WhenGetAllCategory_ThenReturnIterable() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "private", 2, "Manoj", "Manoj", newaccount, role);
		Category newcategory = new Category(UUID.fromString("5b89c108-d327-486b-aa2d-4b88c1889869"), "Credit card",
				"HDFC Credit card payments", new BigDecimal(123456.20), "Manoj", "Manoj", newforecast);
		Category newcategory2 = new Category(UUID.fromString("c91b6732-a8e4-46b7-9979-7bb905c36bca"), "debit card",
				"ICICI Credit card payments", new BigDecimal(8791456.20), "Ravi", "Ravi", newforecast2);
		ArrayList<Category> list = new ArrayList<Category>();
		list.add(newcategory);
		list.add(newcategory2);
		Mockito.when(icategoryrepository.findAll()).thenReturn(list);
		Iterable<Category> expected = categoryserviceimpl.getAllCategories();
		Assertions.assertEquals(expected, list);
	}

	@Test
	@DisplayName("When save forecast is given it should return the saved forecast")
	public void WhenSaveNewForecast_ThenSave() {
		// Positive case
		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Category newcategory = new Category(UUID.fromString("5b89c108-d327-486b-aa2d-4b88c1889869"), "Credit card",
				"HDFC Credit card payments", new BigDecimal(123456.20), "Manoj", "Manoj", newforecast);
		Mockito.when(template.send("test", newcategory)).thenReturn(null);
//		categoryserviceimpl.createNewCategory(newcategory);
//		Mockito.verify(icategoryrepository).save(newcategory);
		template.send("test", newcategory);
		Mockito.verify(template).send("test", newcategory);
	}
}
