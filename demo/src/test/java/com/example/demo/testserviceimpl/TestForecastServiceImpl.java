
package com.example.demo.testserviceimpl;

import java.awt.List;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IAccountRepository;
import com.example.demo.repository.IForecastRepository;
import com.example.demo.repository.IUserDetailsRepository;
import com.example.demo.repository.IUserRoleRepository;
import com.example.demo.serviceimpl.ForecastServiceImpl;

@SpringBootTest
public class TestForecastServiceImpl {

	@InjectMocks
	private ForecastServiceImpl forecastservimpl;

	@Mock
	private IForecastRepository iforecastrepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all forecast details is given it should return an iterable of type forecast")
	public void WhenGetAllForecast_ThenReturnIterable() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public", 2, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "public", 2, "Manoj", "Manoj", newaccount, role);
		ArrayList<Forecast> list = new ArrayList<Forecast>();
		list.add(newforecast);
		list.add(newforecast2);
		Mockito.when(iforecastrepository.findAll()).thenReturn(list);
		Iterable<Forecast> expected = forecastservimpl.getAllForecasts();
		Assertions.assertEquals(expected, list);
	}

	@Test
	@DisplayName("When get forecast by role is given it should return the forecast list")
	public void WhenGetForecastsForRole_ThenReturnIterable() {
		// Positive case
//		String string = new String("4dff16df-2771-4e8b-aee8-22036921c057");
//		UUID uuid = UUID.fromString(string);
//		Assertions.assertNotNull(uuid);

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "private", 2, "Manoj", "Manoj", newaccount, role);

		ArrayList<Forecast> list = new ArrayList<Forecast>();
		list.add(newforecast);
		list.add(newforecast2);
		Mockito.when(iforecastrepository.findForecastsForRole(role.getRoleID(), "public"))
				.thenReturn(list.subList(0, 1));

		Iterable<Forecast> expected = forecastservimpl.findForecastsForRole(role.getRoleID(), "public");
		Assertions.assertEquals(expected, list.subList(0, 1));
//		Optional<Forecast> forecast = iforecastrepository.findById(newforecast.getForecastID());
//		Mockito.when(forecastservimpl.getForecastByID(newforecast.getForecastID())).thenReturn(newforecast);
		// <Forecast> actual =
		// iforecastrepository.findById(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"));
//		Assertions.assertNotNull(actual.get());

	}


	@Test
	@DisplayName("When save forecast is given it should return the saved forecast")
	public void WhenSaveNewForecast_ThenSave() {
		// Positive case

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Mockito.when(template.send("test", newforecast)).thenReturn(null);		
//		Mockito.when(iforecastrepository.save(newforecast)).thenReturn(newforecast);
//		Assertions.assertEquals(forecastservimpl.createNewForecast(newforecast), newforecast);
//		Mockito.verify(iforecastrepository).save(newforecast);
		template.send("test", newforecast);
		Mockito.verify(template).send("test", newforecast);
	}

	@Test
	@DisplayName("When delete forecast is given it should perform delete forecast")
	public void WhenDeleteForecast_ThenDelete() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Forecast forecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
//		forecastservimpl.createNewForecast(forecast);
		Map<String, Boolean> value = new HashMap<String, Boolean>();
		value.put("Deleted", true);
		Mockito.doNothing().when(iforecastrepository).deleteForecast("public", "New20");
//		Map<String, Boolean> expected = forecastservimpl.deleteForecast("public", "New20");
//		Mockito.verify(iforecastrepository).deleteForecast("public", "New20");
//		Assertions.assertEquals(expected, value);
//		Map<String, Boolean> expected = forecastservimpl.deleteForecast("public", "New20");
//		String visibility = new String("Public");
//		String forecastName = new String("new5");
//		Assertions.assertTrue(iforecastrepository.existsById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9")));

//		Mockito.verify(forecastservimpl).deleteForecast(new String("Public"), new String("new12"));
//		Mockito.when(forecastservimpl.deleteForecast(new String("Public"), new String("new12"))).thenReturn(new HashMap<String, Boolean>());
	}

	@Test
	@DisplayName("When get forecast by id is given it should return the forecast")
	public void WhenGetForecastById_ThenReturnObject() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Optional<Forecast> newforecast = Optional
				.ofNullable(new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
						"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role));
		Optional<Forecast> newforecast2 = Optional
				.ofNullable(new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
						"new 12th forecast", "public",2, "Manoj", "Manoj", newaccount, role));

		Mockito.when(iforecastrepository.findById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9")))
				.thenReturn(newforecast);
		Mockito.when(iforecastrepository.findById(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057")))
				.thenReturn(newforecast2);

		Forecast expected = forecastservimpl.getForecastByID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"));
		Forecast expected2 = forecastservimpl.getForecastByID(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"));

		Assertions.assertEquals(newforecast.isPresent(), expected.toString().equals(newforecast.get().toString()));
		Assertions.assertTrue(expected2.toString().equals(newforecast2.get().toString()));
	}

	@Test
	@DisplayName("When invalid forecast id is given it should not return the forecast")
	public void WhenGetForecastByIdNotPresent_ThenReturnObject() {
		Assertions.assertThrows(NoSuchElementException.class, () -> {
			forecastservimpl.getForecastByID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"));
		});
//		PowerMockito.when(iforecastrepository.findById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"))).thenThrow(NoSuchElementException.class));
//		Assertions.assertTrue(forecastservimpl.getForecastByID(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9")).cThrow(NoSuchElementException.class);
//		Forecast expected = forecastservimpl.getForecastByID(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"));		
	}

	@Test
	@DisplayName("When update forecast by id is given it should return the updated forecast")
	public void WhenUpdateForecastById_ThenReturnUpdatedForecast() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		
		Optional<Forecast> forecast = Optional
				.ofNullable(new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
						"new 20th forecast", "public", 12, "Manoj", "Manoj", newaccount, role));
		Forecast newforecast2 = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New14",
				"new 14th forecast", "public",12, "Manoj", "Manoj", newaccount, role);
		ArrayList<String> list = new ArrayList<String>();
		list.add("New20");
		list.add("New21");
		list.add("New22");
		Mockito.when(template.send("test", newforecast2)).thenReturn(null);
//		Mockito.when(iforecastrepository.findForecastName(newforecast2.getAccountID())).thenReturn(list);
//		Mockito.when(iforecastrepository.findById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9")))
//				.thenReturn(forecast);
//		Mockito.when(iforecastrepository.save(newforecast2)).thenReturn(newforecast2);
		template.send("test", newforecast2);
//		forecastservimpl.updateForecastDetails(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), newforecast2);
//		Assertions.assertEquals(expected,newforecast2);
//		Mockito.when(iforecastrepository.save(newforecast2)).thenReturn(newforecast2);
//		Mockito.verify(iforecastrepository, Mockito.atLeastOnce()).findForecastName(newforecast2.getAccountID());
//		Mockito.verify(iforecastrepository, Mockito.atLeastOnce()).findById(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"));
//		Mockito.verify(iforecastrepository, Mockito.atLeastOnce()).save(forecast.get());
		Mockito.verify(template).send("test", newforecast2);
	}
}
