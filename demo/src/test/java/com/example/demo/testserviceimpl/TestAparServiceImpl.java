
package com.example.demo.testserviceimpl;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Apar;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IAparRepository;
import com.example.demo.serviceimpl.AparServiceImpl;

@SpringBootTest
public class TestAparServiceImpl {

	@InjectMocks
	private AparServiceImpl aparserviceimpl;

	@Mock
	private IAparRepository iaparrepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all transaction details is given it should return an iterable of type apar")
	public void WhenGetAllTransaction_ThenReturnIterable() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		AccountDetails newaccount2 = new AccountDetails(UUID.fromString("eeba682b-1bd4-4a54-a12e-ad9e1dd8f8c7"),
				"100000000010", "Kumar", "Gold", "Mexico", "Pesos", "Kumar", "Kumar", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Forecast newforecast2 = new Forecast(UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057"), "New12",
				"new 12th forecast", "private",2, "Manoj", "Manoj", newaccount, role);
		Category newcategory = new Category(UUID.fromString("5b89c108-d327-486b-aa2d-4b88c1889869"), "Credit card",
				"HDFC Credit card payments", new BigDecimal(123456.20), "Manoj", "Manoj", newforecast);
		Category newcategory2 = new Category(UUID.fromString("c91b6732-a8e4-46b7-9979-7bb905c36bca"), "debit card",
				"ICICI Credit card payments", new BigDecimal(8791456.20), "Ravi", "Ravi", newforecast2);
		Apar newtransaction = new Apar(UUID.fromString("805f371c-f49f-4095-bc4e-579102884ddc"), "Manoj",
				new BigDecimal(123456.20), "payable", "Manoj", newcategory, newaccount, newaccount2);
		Apar newtransaction2 = new Apar(UUID.fromString("b41b0839-5b7f-456e-a6ae-7545651ec40b"), "Ravi",
				new BigDecimal(123456.20), "payable", "Manoj", newcategory, newaccount, newaccount2);
		ArrayList<Apar> list = new ArrayList<Apar>();
		list.add(newtransaction);
		list.add(newtransaction2);
		Mockito.when(iaparrepository.findAll()).thenReturn(list);
		Iterable<Apar> expected = aparserviceimpl.getAllTransactions();
		Assertions.assertEquals(expected, list);
	}

	@Test
	@DisplayName("When save transaction is given it should return the saved transaction")
	public void WhenSaveNewTransaction_ThenSave() {
		// Positive case
		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		AccountDetails newaccount2 = new AccountDetails(UUID.fromString("eeba682b-1bd4-4a54-a12e-ad9e1dd8f8c7"),
				"100000000010", "Kumar", "Gold", "Mexico", "Pesos", "Kumar", "Kumar", user);
		Forecast newforecast = new Forecast(UUID.fromString("0b8bc709-684b-45ab-86bd-e403edf0f4a9"), "New20",
				"new 20th forecast", "public",2, "Manoj", "Manoj", newaccount, role);
		Category newcategory = new Category(UUID.fromString("5b89c108-d327-486b-aa2d-4b88c1889869"), "Credit card",
				"HDFC Credit card payments", new BigDecimal(123456.20), "Manoj", "Manoj", newforecast);
		Apar newtransaction = new Apar(UUID.fromString("805f371c-f49f-4095-bc4e-579102884ddc"), "Manoj",
				new BigDecimal(123456.20), "payable", "Manoj", newcategory, newaccount, newaccount2);
		Mockito.when(template.send("test", newtransaction)).thenReturn(null);
//		aparserviceimpl.createNewTrasaction(newtransaction);
//		Mockito.verify(iaparrepository).save(newtransaction);
		template.send("test", newtransaction);
		Mockito.verify(template).send("test", newtransaction);
	}
}
