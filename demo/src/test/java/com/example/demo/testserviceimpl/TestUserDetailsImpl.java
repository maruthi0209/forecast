
package com.example.demo.testserviceimpl;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IUserDetailsRepository;
import com.example.demo.repository.IUserRoleRepository;
import com.example.demo.serviceimpl.UserDetailsImpl;

@SpringBootTest
public class TestUserDetailsImpl {

	@InjectMocks
	private UserDetailsImpl userdetailsimpl;

	@Mock
	private IUserDetailsRepository iuserdetailsrepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all user details is given it should return an iterable of type userdetails")
	public void WhenGetAllUsers_ThenReturnIterable() {
		
		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		UserDetails user2 = new UserDetails(UUID.fromString("eeba682b-1bd4-4a54-a12e-ad9e1dd8f8c7"), "Manoj", role);
		ArrayList<UserDetails> list = new ArrayList<UserDetails>();
		list.add(user);
		list.add(user2);
		Mockito.when(iuserdetailsrepository.findAll()).thenReturn(list);
		Iterable<UserDetails> expected = userdetailsimpl.getAllUsers();
		Assertions.assertEquals(list, expected);
	}

	@Test
	@DisplayName("when new userdetail is saved, it should be saved through the repository")
	public void WhenNewUser_ThenReturnObject() {

		UserDetails newuser = new UserDetails();
		newuser.setUserID(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"));
		newuser.setUserName("Manoj");
		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		newuser.setRoleID(role);
		Mockito.when(template.send("test", newuser)).thenReturn(null);
//		userdetailsimpl.createNewUser(newuser);
//		Mockito.verify(iuserdetailsrepository).save(newuser);
		template.send("test", newuser);
		Mockito.verify(template).send("test", newuser);
	}

}
