
package com.example.demo.testserviceimpl;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.UserDetails;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IAccountRepository;
import com.example.demo.serviceimpl.AccountServiceImpl;

@SpringBootTest
public class TestAccountServiceImpl {

	@InjectMocks
	private AccountServiceImpl accountserviceimpl;

	@Mock
	private IAccountRepository iaccountrepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all account details is given it should return an iterable of type accountdetails")
	public void WhenGetAllAccounts_ThenReturnIterable() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		AccountDetails newaccount2 = new AccountDetails(UUID.fromString("eeba682b-1bd4-4a54-a12e-ad9e1dd8f8c7"),
				"100000000010", "Kumar", "Gold", "Mexico", "Pesos", "Kumar", "Kumar", user);
		ArrayList<AccountDetails> list = new ArrayList<AccountDetails>();
		list.add(newaccount);
		list.add(newaccount2);
		Mockito.when(iaccountrepository.findAll()).thenReturn(list);
		Iterable<AccountDetails> expected = accountserviceimpl.getAllAccounts();
		Assertions.assertEquals(list, expected);
	}

	@Test
	@DisplayName("when new accountdetails is saved, it should be saved by repository")
	public void WhenNewAccount_ThenReturnObject() {

		UserRole role = new UserRole(UUID.fromString("b1655bc9-5b08-4715-91bb-d901efe7218b"), "Regular");
		UserDetails user = new UserDetails(UUID.fromString("9d8ad1b7-3588-41ae-92ba-8502ec81f0d8"), "Manoj", role);
		AccountDetails newaccount = new AccountDetails(UUID.fromString("572c490b-8b6e-4fef-b268-c1da69b451b0"),
				"100000000012", "Manoj", "Platinum", "China", "Yuan", "Manoj", "Manoj", user);
		Mockito.when(template.send("test", newaccount)).thenReturn(null);
//		accountserviceimpl.createNewAccount(newaccount);
//		Mockito.verify(iaccountrepository).save(newaccount);
		template.send("test", newaccount);
		Mockito.verify(template).send("test", newaccount);

	}

}
