
package com.example.demo.testserviceimpl;

import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import com.example.demo.exception.UserRoleNotFoundException;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IUserRoleRepository;
import com.example.demo.serviceimpl.UserRoleImpl;

@SpringBootTest
public class TestUserRoleImpl {

	@InjectMocks
	private UserRoleImpl userroleimpl;

	@Spy
	private IUserRoleRepository iuserrolerepository;
	
	@Mock
	private KafkaTemplate<String, Object> template;

	@BeforeEach
	public void setMockSetup() {

		MockitoAnnotations.openMocks(this);
	}

	@Test
	@DisplayName("When get all roles is given it should return an iterable of type userrole")
	public void WhenGetAllRoles_ThenReturnIterable() {

		UserRole newrole = new UserRole(UUID.fromString("f6686489-d291-4ff2-88fb-f80c775449b6"), "Exclusive");
		UserRole newrole2 = new UserRole(UUID.fromString("eeba682b-1bd4-4a54-a12e-ad9e1dd8f8c7"), "Premium");
		ArrayList<UserRole> list = new ArrayList<UserRole>();
		list.add(newrole);
		list.add(newrole2);
		Mockito.when(iuserrolerepository.findAll()).thenReturn(list);
		Iterable<UserRole> expected = userroleimpl.getAllRoles();
		Assertions.assertEquals(list, expected);

	}

	@Test
	@DisplayName("when new role is saved, it should the saved object in return")
	public void WhenNewRole_ThenReturnObject() {

		UserRole newrole = new UserRole(UUID.fromString("f6686489-d291-4ff2-88fb-f80c775449b6"), "Exclusive");
		Mockito.when(template.send("test", newrole)).thenReturn(null);
//		userroleimpl.createNewRole(newrole);
//		Mockito.verify(iuserrolerepository).save(newrole);
		template.send("test", newrole);
		Mockito.verify(template).send("test", newrole);;
	}
}
