
package com.example.demo.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.pojos.AccountDetails;

/**
 * This interface is used for CRUD operations related to Account
 * 
 * @author schennapragada
 *
 */
@Repository
public interface IAccountRepository extends CrudRepository<AccountDetails, UUID> {
	// Implementing CRUD operation for AccountDetails

	/*
	 * @Query("SELECT accountID, acHolderName, accountNumber, accountType, country, createdBy, createdTime, currency, updatedBy, updatedTime, userID FROM AccountDetails WHERE accountID=?1"
	 * ) AccountDetails getDetailsByID(UUID accountID);
	 */
}
