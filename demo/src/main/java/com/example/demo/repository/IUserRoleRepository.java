
package com.example.demo.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.pojos.UserRole;

/**
 * This interface is used for CRUD operations related to User Role
 * 
 * @author schennapragada
 *
 */
@Repository
public interface IUserRoleRepository extends CrudRepository<UserRole, UUID> {

}