package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.pojos.MyUser;

public interface MyUserRepo extends CrudRepository<MyUser, String> {

}
