
package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.pojos.Apar;
import com.example.demo.pojos.Category;

/**
 * This interface is used for CRUD operations related to Transactions
 * 
 * @author schennapragada
 *
 */
@Transactional
public interface IAparRepository extends CrudRepository<Apar, UUID> {
	// Implementing CRUD operations for Transactions

	/**
	 *  Query for getting all transaction ids for given category id
	 * @param cateogryID
	 * @return List of type UUIDs
	 */
	@Modifying
	@Query("DELETE FROM Apar WHERE categoryID=?1")
	void deleteAparForCategory(Category category);

}
