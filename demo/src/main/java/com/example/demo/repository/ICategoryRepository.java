
package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;

/**
 * This interface is used for CRUD operations related to Category
 * 
 * @author schennapragada
 *
 */
public interface ICategoryRepository extends CrudRepository<Category, UUID> {
	// Implementing CRUD operations for Categories
	/**
	 *  Query for getting all transaction ids for given category id
	 * @param cateogryID
	 * @return List of type UUIDs
	 */
	@Query("DELETE FROM Category WHERE forecastID=?1")
	void deleteCategoryForForecast(Forecast forecastID);
	
	@Query("SELECT categoryID FROM Category WHERE forecastID=?1")
	List<UUID> findCategoryForForecast(Forecast forecastID);
}
