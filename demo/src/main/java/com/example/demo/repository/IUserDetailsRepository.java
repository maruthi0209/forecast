
package com.example.demo.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.pojos.UserDetails;

/**
 * This interface is used for CRUD operations related to User Details
 * 
 * @author schennapragada
 *
 */
@Repository
public interface IUserDetailsRepository extends CrudRepository<UserDetails, UUID> {

}
