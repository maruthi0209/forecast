
package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Forecast;

/**
 * This interface is used for CRUD operations related to Forecasts
 * 
 * @author schennapragada
 *
 */
@Transactional
public interface IForecastRepository extends CrudRepository<Forecast, UUID> {
	// Implementing CRUD operations for Forecasts

	/**
	 *  Query to find all the forecasts for given role
	 * @param roleID
	 * @param visibility
	 * @return Iterable of type Forecast
	 */
	@Query("SELECT forecastID, forecastName, forecastDescription, horizonPeriod FROM Forecast WHERE roleid = ?1 and visibility = ?2")
	Iterable<Forecast> findForecastsForRole(UUID roleID, String visibility);

	/*
	 * @Query("SELECT forecastID, forecastName, forecastDescription, horizonPeriod FROM Forecast WHERE visibility = 'Public'"
	 * ) Iterable<Forecast> findPublicForecast();
	 */

	/**
	 *  Query to find all the forecast names for given accountID
	 * @param accountID
	 * @return List of type String 
	 */
	@Query("SELECT forecastName FROM Forecast WHERE accountID = ?1")
	ArrayList<String> findForecastName(AccountDetails accountID);

	/**
	 *  Query to delete forecasts based on name and visibility
	 * @param visibility
	 * @param forecastName
	 */
	@Modifying
	@Query("DELETE FROM Forecast WHERE visibility = ?1 and forecastName= ?2")
	void deleteForecast(String visibility, String forecastName);
	
	/**
	 *  
	 * @param forecastName
	 * @return
	 */
	@Query("SELECT forecastID FROM Forecast WHERE forecastName = ?1")
	UUID findForecastID(String forecastName);

}
