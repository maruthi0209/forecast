package com.example.demo.exception;

public class ForecastCannotBeDeleted extends RuntimeException {

	/** Exception when the forecast to be 
	 *  deleted is private 
	 */
	private static final long serialVersionUID = -5812233741658802641L;

	public ForecastCannotBeDeleted() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ForecastCannotBeDeleted(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ForecastCannotBeDeleted(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForecastCannotBeDeleted(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ForecastCannotBeDeleted(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
