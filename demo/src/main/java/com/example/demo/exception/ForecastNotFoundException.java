package com.example.demo.exception;

public class ForecastNotFoundException extends RuntimeException
{

	/** Exception when the forecast with given name 
	 *  is not visible or there are no available 
	 *  forecasts in findall method
	 */
	private static final long serialVersionUID = -3350621600621346008L;

	public ForecastNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ForecastNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ForecastNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForecastNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ForecastNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
