package com.example.demo.exception;

public class UserRoleNotFoundException extends RuntimeException {

	/** Exception when there is no user role 
	 *  returned by the find all method in controller
	 */
	private static final long serialVersionUID = -9109054593056971190L;

	public UserRoleNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserRoleNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserRoleNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserRoleNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserRoleNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
