package com.example.demo.exception;

public class TransactionAlreadyExists extends RuntimeException {

	/**Exception when the transaction with given id 
	 *  is already available 
	 */
	private static final long serialVersionUID = -94146176715559734L;

	public TransactionAlreadyExists() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransactionAlreadyExists(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TransactionAlreadyExists(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TransactionAlreadyExists(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TransactionAlreadyExists(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
