package com.example.demo.exception;

public class CategoryNotFoundException extends RuntimeException {

	/** Exception when there are no categories 
	 *  to be returned in the find all method for 
	 *  category controller
	 */
	private static final long serialVersionUID = 2761997107786549422L;

	public CategoryNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CategoryNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CategoryNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CategoryNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CategoryNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
