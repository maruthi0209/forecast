package com.example.demo.exception;

public class ForecastAlreadyExists extends RuntimeException {

	/** Exception when a forecast with name already 
	 *  exists for given user
	 */
	private static final long serialVersionUID = 8381660717501407755L;

	public ForecastAlreadyExists() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ForecastAlreadyExists(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ForecastAlreadyExists(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForecastAlreadyExists(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ForecastAlreadyExists(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
