package com.example.demo.exception;

public class TransactionNotFoundException extends RuntimeException
{

	/** Exception when the find all method in 
	 *  apar controller returns no values
	 * 
	 */
	private static final long serialVersionUID = 7013895625327259163L;

	public TransactionNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransactionNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public TransactionNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public TransactionNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public TransactionNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
