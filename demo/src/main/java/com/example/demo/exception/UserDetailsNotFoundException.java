package com.example.demo.exception;

public class UserDetailsNotFoundException extends RuntimeException
{

	/** Exception when there are no user details 
	 *  returned by find all method in the controller
	 */
	private static final long serialVersionUID = 486505861663250970L;

	public UserDetailsNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDetailsNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public UserDetailsNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserDetailsNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserDetailsNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
