

package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;

import com.example.demo.pojos.StandardReleaseResponse;

public class EntitlementUtil {
	
	/**
	 *  Class for defining the Http response error code 
	 * @param standardResponse
	 * @param code
	 * @param message
	 * @return
	 */

	public static StandardReleaseResponse  createReleaseResponse(final StandardReleaseResponse standardResponse,
			final String code, final String message) {
		standardResponse.setResponsecode(code);
		standardResponse.setResponsemessage(message);
		return standardResponse;
	}

	public static BodyBuilder getBodyBuilder(final String errorCode) {
		final HttpStatus httpStatus = formulateResponse(errorCode);
		return ResponseEntity.status(httpStatus);
	}
	
	public static HttpStatus formulateResponse(final String code) {
		HttpStatus response;
		switch (code) {
		case "200":
			response = HttpStatus.valueOf("OK");
			break;
		case "201":
			response = HttpStatus.valueOf("CREATED");
			break;
		/*case "202":
			response = HttpStatus.valueOf("ACCEPTED");
			break;*/
		case "400":
			response = HttpStatus.valueOf("BAD_REQUEST");
			break;
		case "401":
			response = HttpStatus.valueOf("UNAUTHORIZED");
			break;
		case "403":
			response = HttpStatus.valueOf("FORBIDDEN");
			break;
		case "404":
			response = HttpStatus.valueOf("NOT_FOUND");
			break;
		case "409":
			response = HttpStatus.valueOf("CONFLICT");
			break;
		case "412":
			response = HttpStatus.valueOf("PRECONDITION_FAILED");
			break;
		case "422":
			response = HttpStatus.valueOf("UNPROCESSABLE_ENTITY");
			break;
		/*case "500":
			response = HttpStatus.valueOf("INTERNAL_SERVER_ERROR");
			break;*/
		case "501":
			response = HttpStatus.valueOf("NOT_IMPLEMENTED");
			break;
		case "504":
			response = HttpStatus.valueOf("GATEWAY_TIMEOUT");
			break;
		default:
			response = HttpStatus.valueOf("INTERNAL_SERVER_ERROR");
			break;
		}
		return response;
	}
}

