
package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.UserDetailsNotFoundException;
import com.example.demo.pojos.UserDetails;
import com.example.demo.service.IUserDetailsService;

/**
 * This is a controller class for the User details module which maps the request
 * to service functions
 * 
 * @author schennapragada
 *
 */

@RestController
public class UserDetailsController {

	/**
	 * Creating an instance of IUserDetailsService interface
	 */
	@Autowired
	private IUserDetailsService iuserdetailsservice;

	/**
	 * Method for creating a new user details entry into the database
	 * 
	 * @param userdetails
	 * @return object of type UserDetails
	 */
	@PostMapping("/newuserdetails")
	public UserDetails createNewUser(@Validated @RequestBody UserDetails userdetails) {
		return iuserdetailsservice.createNewUser(userdetails);
	}

	/**
	 * Method for returning all the available users details in the database
	 * 
	 * @return Iterable of type UserDetails
	 */
	@GetMapping("/alluserdetails")
	public Iterable<UserDetails> getAllUsers() {
		return iuserdetailsservice.getAllUsers();
	}

}
