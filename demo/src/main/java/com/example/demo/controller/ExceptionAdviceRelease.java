
package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.example.demo.exception.EntitlementUtil;
import com.example.demo.pojos.StandardReleaseResponse;

@ControllerAdvice(assignableTypes = ExceptionController.class)
public class ExceptionAdviceRelease {

	@ExceptionHandler(value = RuntimeException.class)
	protected ResponseEntity<StandardReleaseResponse> handleRestOfExceptions(RuntimeException ex) {
		final StandardReleaseResponse releaseResponse = new StandardReleaseResponse();
		releaseResponse.setResponsecode("400");
		releaseResponse.setResponsemessage("unable to process request");
		final HttpStatus httpStatus = EntitlementUtil.formulateResponse(releaseResponse.getResponsecode());
		final BodyBuilder body = ResponseEntity.status(httpStatus);
		return body.body(releaseResponse);

	}
}
