
package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.AccountNotFoundException;
import com.example.demo.pojos.AccountDetails;
import com.example.demo.service.IAccountService;

/**
 * This is a controller class for the accounts module which maps the request to
 * service methods
 * 
 * @author schennapragada
 *
 */

@RestController
public class AccountController {

	/**
	 * Creating an instance of accountservice interface
	 */
	@Autowired
	private IAccountService iaccountservice;

	/**
	 * Welcome page for this application
	 * 
	 * @return Greeting message
	 */
	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	/**
	 * Request Mapping to a method for Creating a new account
	 * 
	 * @param accountdetails
	 * @return object of type AccountDetails
	 */
	@PostMapping("/newaccount")
	public AccountDetails createNewAccount(@Validated @RequestBody AccountDetails accountdetails) {
		return iaccountservice.createNewAccount(accountdetails);

	}

	/**
	 * Method to get info about all the users available in the database
	 * 
	 * @return Iterable of type AccountDetails
	 */
	@GetMapping("/allaccounts")
	public Iterable<AccountDetails> getAllAccounts() {

		return iaccountservice.getAllAccounts();
	}
}
