
package com.example.demo.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ForecastAlreadyExists;
import com.example.demo.exception.ForecastNotFoundException;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.StandardReleaseResponse;
import com.example.demo.service.IForecastService;

/**
 * This is a controller class for the forecasts module which maps the request to
 * service methods
 * 
 * @author schennapragada
 *
 */

@RestController
public class ForecastController {

	/**
	 * Creating an instance of forecast service interface
	 */
	@Autowired
	private IForecastService iforecastservice;

	/**
	 * Method for creating a new forecast
	 * 
	 * @param forecast object
	 * @return object of type forecast
	 * @throws ParseException
	 */
	@PostMapping("/newforecast")
	public Forecast createNewforecast(@Validated @RequestBody Forecast forecast) {
		/*
		 * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		 * forecast.setHorizonPeriod(TimeUnit.MILLISECONDS.toDays(
		 * (forecast.getEndDate() - forecast.getStartDate()) % 7);
		 */
//		List<String> list = iforecastservice.findForecastName(forecast.getAccountID());
//		if (iforecastservice.existsByID(forecast.getForecastID()) || list.contains(forecast.getForecastName())) {
//			throw new ForecastAlreadyExists();
//		}
//		{
			return iforecastservice.createNewForecast(forecast);
//		}

	}

	/**
	 * Method to get all the available forecast in the database
	 * 
	 * @return Iterable of type Forecast
	 */
	@GetMapping("/allforecasts")
	public Iterable<Forecast> getAllForecasts() {
		if (iforecastservice.getAllForecasts() == null) {
			throw new ForecastNotFoundException();
		} else {
			return iforecastservice.getAllForecasts();
		}
	}

	/**
	 * Method to get a particular forecast detail from the database
	 * 
	 * @param forecastID
	 * @return Response Entity of type Forecast
	 */
	@GetMapping("/forecast/{forecastID}")
	public ResponseEntity<Forecast> getForecastID(@PathVariable(value = "forecastID") UUID forecastID) {
		Forecast forecast = iforecastservice.getForecastByID(forecastID);
		if (forecast != null) // checks if the forecast with this id exists or not
		{
		return ResponseEntity.ok().body(forecast);
		}
		{
			throw new ForecastNotFoundException();
		}
	}

	/**
	 * Method for getting forecasts from role and visibility
	 * 
	 * @param roleID
	 * @param visibility
	 * @return Iterable of type Forecast
	 */
	@GetMapping("/getbyrole/{roleid}/{visibility}")
	public Iterable<Forecast> findForecastsForRole(@PathVariable(value = "roleid") UUID roleID,
			@PathVariable(value = "visibility") String visibility) throws StandardReleaseResponse {
		return iforecastservice.findForecastsForRole(roleID, visibility);
	}

	/**
	 * Method to update a particular forecast detail from the database based on
	 * forecase id
	 * 
	 * @param forecastID
	 * @return Response Entity of type Forecast
	 */
	@PutMapping("/updateforecast/{forecastID}")
	public ResponseEntity<Forecast> updateForecastDetails(@PathVariable(value = "forecastID") UUID forecastID,
			@Validated @RequestBody Forecast forecast) {
		Forecast updatedforecast = iforecastservice.updateForecastDetails(forecastID, forecast);
		return ResponseEntity.ok(updatedforecast);
	}

	/**
	 * Method for deleting a forecast from the database
	 * 
	 * @param forecastID
	 * @return Map of String and Boolean
	 */
	/*
	 * @DeleteMapping("/deleteForecast/{forecastName}") public Map<String, Boolean>
	 * deleteForecast(@PathVariable(value = "forecastName") UUID forecastID) {
	 * Map<String, Boolean> outerresponse =
	 * iforecastservice.deleteForecast(forecastID); return outerresponse; }
	 */

	/**
	 * Method for deleting forecasts based on public visibility and name
	 */
	@DeleteMapping("/deleteforecast/{visibility}/{forecastName}")
	public Map<String, Boolean> deleteForecast(@PathVariable(value = "visibility") String visibility,
			@PathVariable(value = "forecastName") String forecastName) {
		return iforecastservice.deleteForecast(visibility, forecastName);

	}

}
