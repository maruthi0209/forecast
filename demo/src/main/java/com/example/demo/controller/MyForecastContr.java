package com.example.demo.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.ForecastNotFoundException;
import com.example.demo.pojos.MyForecast;
import com.example.demo.service.MyForecastService;
/**
 *  Experimental controller for rough work
 * @author schennapragada
 *
 */
@RestController
public class MyForecastContr {
	@Autowired
	private MyForecastService myforecastservice;

	@PostMapping("/MyForecast")
	public MyForecast myForecast(@Validated @RequestBody MyForecast myforecast) {
		return myforecastservice.addForecast(myforecast);
	}

	@GetMapping("/dummy")
	public UUID getFore() {
//		if (myforecastservice.getFore().toString() == "[]") {
//			throw new ForecastNotFoundException();
//		} else {
			return UUID.fromString("4dff16df-2771-4e8b-aee8-22036921c057");
//		}

	}
}
