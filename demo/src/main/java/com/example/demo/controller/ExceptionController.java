package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.exception.AccountNotFoundException;
import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.exception.ForecastAlreadyExists;
import com.example.demo.exception.ForecastCannotBeDeleted;
import com.example.demo.exception.ForecastNotFoundException;
import com.example.demo.exception.TransactionAlreadyExists;
import com.example.demo.exception.TransactionNotFoundException;
import com.example.demo.exception.UserDetailsNotFoundException;
import com.example.demo.exception.UserRoleNotFoundException;
import com.example.demo.pojos.StandardReleaseResponse;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = TransactionNotFoundException.class)
	public ResponseEntity<Object> handleTransactionExceptions(TransactionNotFoundException exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("Transaction Not found");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;
	}

	@ExceptionHandler(value = CategoryNotFoundException.class)
	public ResponseEntity<Object> handleCategoryExceptions(CategoryNotFoundException exception, WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage(exception.getMessage());
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;
	}

	@ExceptionHandler(value = ForecastNotFoundException.class)
	public ResponseEntity<StandardReleaseResponse> handleForecastExceptions(ForecastNotFoundException exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("Forecast Not found");
		ResponseEntity<StandardReleaseResponse> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;

	}

	@ExceptionHandler(value = AccountNotFoundException.class)
	public ResponseEntity<Object> handleAccountDetailsExceptions(AccountNotFoundException exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("Accounts Not found");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;
	}

	@ExceptionHandler(value = UserDetailsNotFoundException.class)
	public ResponseEntity<Object> handleUserDetailsExceptions(UserDetailsNotFoundException exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("UserDetails Not found");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;
	}

	@ExceptionHandler(value = UserRoleNotFoundException.class)
	public ResponseEntity<Object> handleUserRoleExceptions(UserRoleNotFoundException exception, WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("UserRoles Not found");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
		return entity;
	}

	@ExceptionHandler(value = ForecastCannotBeDeleted.class)
	public ResponseEntity<Object> handleForecastDeleteExceptions(ForecastCannotBeDeleted exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("403");
		response.setResponsemessage("Forecast cannot be deleted");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
		return entity;
	}

	@ExceptionHandler(value = ForecastAlreadyExists.class)
	public ResponseEntity<Object> handleForecastExistsExceptions(ForecastAlreadyExists exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("403");
		response.setResponsemessage("Forecast already exists");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
		return entity;
	}

	@ExceptionHandler(value = TransactionAlreadyExists.class)
	public ResponseEntity<Object> handleTransactionExistsExceptions(ForecastAlreadyExists exception,
			WebRequest webRequest) {
		StandardReleaseResponse response = new StandardReleaseResponse();
		response.setResponsecode("450");
		response.setResponsemessage("Transaction already exists");
		ResponseEntity<Object> entity = new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
		return entity;
	}
}
