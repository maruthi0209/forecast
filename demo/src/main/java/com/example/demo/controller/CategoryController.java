
package com.example.demo.controller;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.pojos.Category;
import com.example.demo.service.ICategoryService;

/**
 * This is a controller class for the category module which maps the request to
 * service methods
 * 
 * @author schennapragada
 *
 */

@RestController
public class CategoryController {

	/**
	 * Creating an instance of category service
	 */
	@Autowired
	private ICategoryService icategoryservice;

	/**
	 * Method for creating a new category of transaction
	 * 
	 * @param category
	 * @return object of type Category
	 */
	@PostMapping("/newcategory")
	public Category createnewcategory(@Validated @RequestBody Category category) {
		return icategoryservice.createNewCategory(category);
	}

	/**
	 * Method for getting all the available category of transaction from the
	 * database
	 *  Note: the for block in this method cannot be put 
	 *  	  in categoryserviceimpl because it calls icategoryrepository and 
	 *  	  changes the values in the database. 
	 * 
	 * @return Iterable of type Category
	 */
	@GetMapping("/allcategories")
	public Iterable<Category> getAllCategories() {
		BigDecimal paisa = new BigDecimal("100");
		Iterable<Category> categoryiterable = icategoryservice.getAllCategories();
		for (Category category : categoryiterable) {
			category.setCategoryAmount(category.getCategoryAmount().divide(paisa));
		}
		return categoryiterable;
	}

}
