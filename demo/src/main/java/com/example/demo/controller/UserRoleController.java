
package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.UserRoleNotFoundException;
import com.example.demo.pojos.UserRole;
import com.example.demo.service.IUserRoleService;

/**
 * This is a controller class for the User roles module which maps the request
 * to service functions
 * 
 * @author schennapragada
 *
 */

@RestController
public class UserRoleController {

	/**
	 * Creating an instance of IUserRoleService interface
	 */
	@Autowired
	private IUserRoleService iuserroleservice;

	/**
	 * Method for returning all the available users roles in the database
	 * 
	 * @return Iterable of type UserRoles
	 */
	@GetMapping("/getallroles")
	public Iterable<UserRole> getAllRoles() {
			return iuserroleservice.getAllRoles();
		}


	/**
	 * Method for creating a new user roles entry into the database
	 * 
	 * @return object of type UserRole
	 */
	@PostMapping("/newuserrole")
	public UserRole createNewRole(@Validated @RequestBody UserRole userrole) {
		return iuserroleservice.createNewRole(userrole);
	}

}
