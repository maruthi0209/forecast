
package com.example.demo.controller;

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.TransactionAlreadyExists;
import com.example.demo.exception.TransactionNotFoundException;
import com.example.demo.pojos.Apar;
import com.example.demo.service.IAparService;

/**
 * This is a controller class for the transactions module which maps the request
 * to service methods
 * 
 * @author schennapragada
 *
 */

@RestController
public class AparController {

	/**
	 * Creating an instance of apar service interface
	 */
	@Autowired
	private IAparService iaparservice;

	/**
	 * Method for creating a new transaction into the database
	 * 
	 * @param transaction
	 * @return object of type Apar
	 */
	@PostMapping("/newtransaction")
	public Apar createnewtransaction(@Validated @RequestBody Apar transaction) {
		return iaparservice.createNewTrasaction(transaction);
	}

	/**
	 * Method to get information about all the available transactions in the
	 * database for an account Note: the else block in this method cannot be put in
	 * aparserviceimpl because it calls iaparrepository and change the values in the
	 * database.
	 * 
	 * @return Iterable of type Apar
	 */
	@GetMapping("/alltransactions")
	public Iterable<Apar> getAllTransactions() {

		BigDecimal paisa = new BigDecimal("100");
		Iterable<Apar> apariterable = iaparservice.getAllTransactions();
		for (Apar transaction : apariterable) {
			transaction.setTransactionAmount(transaction.getTransactionAmount().divide(paisa));
		}
		return apariterable;
	}

}
