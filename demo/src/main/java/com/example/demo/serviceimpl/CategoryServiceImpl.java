
package com.example.demo.serviceimpl;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.pojos.Apar;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.repository.IAparRepository;
import com.example.demo.repository.ICategoryRepository;
import com.example.demo.service.IAparService;
import com.example.demo.service.ICategoryService;

/**
 * This class implements the methods defined in the Category service interface
 * by performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class CategoryServiceImpl implements ICategoryService {

	/**
	 * creating an instance of category repository interface for crud operations
	 */
	@Autowired
	private ICategoryRepository icategoryrepository;
	
	@Autowired
	private IAparRepository iaparrepository;
	
	@Autowired
	private KafkaTemplate<String, Object> template;

	/**
	 * Implementation method for getting all the available categories using Category
	 * repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<Category> getAllCategories() {
		if (icategoryrepository.findAll() == null) {
			throw new CategoryNotFoundException();
		} else {
			return icategoryrepository.findAll();
		}
	}

	/**
	 * Implementation method for creating a new category using Category repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Category createNewCategory(Category category) {
		category.setCreatedTime(LocalDate.now());
		category.setUpdatedTime(LocalDate.now());
		ByteBuffer createdbuffer = StandardCharsets.UTF_8.encode(category.getCreatedTime().toString());
		ByteBuffer updatedbuffer = StandardCharsets.UTF_8.encode(category.getUpdatedTime().toString());
		category.setCreatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(createdbuffer)));
		category.setCreatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(updatedbuffer)));
		BigDecimal paisa = new BigDecimal("100");
		category.setCategoryAmount(category.getCategoryAmount().multiply(paisa));
//		template.send("test", category);
//		return category;
		return icategoryrepository.save(category);
		
	}
	
	/**
	 * Implementation method for finding the category ids based on forecast id
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public List<UUID> findCategoryForForecast(Forecast forecastID){
		return icategoryrepository.findCategoryForForecast(forecastID);
	}
	
	/**
	 * Implementation method for deleting categories by id
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Map<String, Boolean> deleteCategory(UUID categoryID) {
		Category category = icategoryrepository.findById(categoryID).orElseThrow();
		iaparrepository.deleteAparForCategory(category);
		icategoryrepository.deleteById(categoryID);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
		}

}
