
package com.example.demo.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.UserRoleNotFoundException;
import com.example.demo.pojos.UserRole;
import com.example.demo.repository.IUserRoleRepository;
import com.example.demo.service.IUserRoleService;

/**
 * This class implements the methods defined in the IUserRole service interface
 * by performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class UserRoleImpl implements IUserRoleService {

	/**
	 * creating an instance of Userrole repository interface for crud operations
	 */

	@Autowired
	private IUserRoleRepository iuserrolerepository;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkatemplate;

	private static final Logger logger = LoggerFactory.getLogger(UserRoleImpl.class);

	/**
	 * Implementation method for getting all the available userroles using userroles
	 * repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<UserRole> getAllRoles() {
		if (iuserrolerepository.findAll()==null) {
			throw new UserRoleNotFoundException();
		} else {
		return iuserrolerepository.findAll();
	}
	}

	/**
	 * Implementation method for creating a new userrole using userrole repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public UserRole createNewRole(UserRole userrole) {
		logger.info("User role data {},{} ", userrole.getRoleDescription(), userrole.getRoleID());
		return iuserrolerepository.save(userrole);
//		kafkatemplate.send("test", userrole);
//		return userrole;
	}
	
	
}
