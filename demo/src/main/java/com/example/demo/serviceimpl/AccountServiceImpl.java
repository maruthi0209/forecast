
package com.example.demo.serviceimpl;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.AccountNotFoundException;
import com.example.demo.pojos.AccountDetails;
import com.example.demo.repository.IAccountRepository;
import com.example.demo.service.IAccountService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

//  
/**
 * This class implements the methods defined in the Account service interface by
 * performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class AccountServiceImpl implements IAccountService {

	/**
	 * creating an instance of account repository interface for crud operations
	 */
	@Autowired
	private IAccountRepository iaccountrepository;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkatemplate;

	/**
	 * Implementation method for getting all the accounts using account repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<AccountDetails> getAllAccounts() {
		if (iaccountrepository.findAll() == null) {
			throw new AccountNotFoundException();
		} else {
			return iaccountrepository.findAll();
		}
	}

	/**
	 * Implementation method for creating a new account using account repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public AccountDetails createNewAccount(AccountDetails accountdetails) {
		accountdetails.setCreatedTime(LocalDate.now());
		accountdetails.setUpdatedTime(LocalDate.now());
//		accountdetails.setCurrency(Currency.getInstance(accountdetails.getCountry()).toString());
		ByteBuffer createdbuffer = StandardCharsets.UTF_8.encode(accountdetails.getCreatedTime().toString());
		ByteBuffer updatedbuffer = StandardCharsets.UTF_8.encode(accountdetails.getUpdatedTime().toString());
		accountdetails.setCreatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(createdbuffer).toString()));
		accountdetails.setUpdatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(updatedbuffer).toString()));
//		kafkatemplate.send("test", accountdetails);
//		return accountdetails;
		return iaccountrepository.save(accountdetails);
	}

}
