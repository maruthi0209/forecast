package com.example.demo.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.pojos.MyForecast;
import com.example.demo.pojos.MyUser;
import com.example.demo.repository.MyForecastRepo;
import com.example.demo.repository.MyUserRepo;
import com.example.demo.service.MyForecastService;

@Transactional(rollbackFor = { Exception.class })
@Service
public class MyForecastServImpl implements MyForecastService {
	@Autowired
	private MyForecastRepo myforecastrepo;

	@Autowired
	private MyUserRepo myuserrepo;

	@Transactional(rollbackFor = { Exception.class })
	public MyForecast addForecast(MyForecast myforecast) {

		myforecastrepo.save(myforecast);
		MyUser myuser = new MyUser();
		myuser.setuID(myforecast.getuID());
		myuserrepo.save(myuser);
		return myforecast;
	}

	@Override
	public Iterable<MyForecast> getFore() {
		return myforecastrepo.findAll();
	}

}
