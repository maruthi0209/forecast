
package com.example.demo.serviceimpl;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.AccountNotFoundException;
import com.example.demo.exception.TransactionNotFoundException;
import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Apar;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.repository.IAparRepository;
import com.example.demo.service.IAccountService;
import com.example.demo.service.IAparService;

/**
 * This class implements the methods defined in the Apar service interface by
 * performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class AparServiceImpl implements IAparService {

	/**
	 * creating an instance of apar repository interface for crud operations
	 */
	@Autowired
	private IAparRepository iaparrepository;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkatemplate;

	/**
	 * Implementation method for getting all the available transactions using apar
	 * repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<Apar> getAllTransactions() {
		if (iaparrepository.findAll() == null) {
			throw new TransactionNotFoundException();
		} else {
			return iaparrepository.findAll();
		}
	}

	/**
	 * Implementation method for creating a new transaction using apar repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Apar createNewTrasaction(Apar transaction) {

		transaction.setCreatedTime(LocalDate.now());
		transaction.setUpdatedTime(LocalDate.now());
		transaction.setTransactionDate(Instant.now());
		ByteBuffer transactionbuffer = StandardCharsets.UTF_8.encode(transaction.getTransactionDate().toString());
		ByteBuffer createdbuffer = StandardCharsets.UTF_8.encode(transaction.getCreatedTime().toString());
		ByteBuffer updatedbuffer = StandardCharsets.UTF_8.encode(transaction.getUpdatedTime().toString());
		transaction.setTransactionDate(Instant.parse(StandardCharsets.UTF_8.decode(transactionbuffer)));
		transaction.setCreatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(createdbuffer)));
		transaction.setUpdatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(updatedbuffer)));
		BigDecimal paisa = new BigDecimal("100");
		transaction.setTransactionAmount(transaction.getTransactionAmount().multiply(paisa));
//		kafkatemplate.send("test", transaction);
//		return transaction;
		return iaparrepository.save(transaction);
		

	}

	/**
	 * Implementation method for deleting transactions by id
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Map<String, Boolean> deleteTransaction(UUID uuid) {
		iaparrepository.deleteById(uuid);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}

	/**
	 *  Implementation method for finding list of transaction id for given category
	 */
	
	@Override
	public Map<String, Boolean> deleteAparForCategory(Category category) {
		iaparrepository.deleteAparForCategory(category);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}


}
