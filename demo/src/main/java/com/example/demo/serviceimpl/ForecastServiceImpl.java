
package com.example.demo.serviceimpl;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.CategoryNotFoundException;
import com.example.demo.exception.ForecastAlreadyExists;
import com.example.demo.exception.ForecastCannotBeDeleted;
import com.example.demo.exception.ForecastNotFoundException;
import com.example.demo.exception.TransactionNotFoundException;
import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.repository.IAparRepository;
import com.example.demo.repository.ICategoryRepository;
import com.example.demo.repository.IForecastRepository;
import com.example.demo.service.ICategoryService;
import com.example.demo.service.IForecastService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements the methods defined in the forecast service interface
 * by performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class ForecastServiceImpl implements IForecastService {

	private static final Logger logger = LoggerFactory.getLogger(ForecastServiceImpl.class);
	/**
	 * creating an instance of forecast repository interface for crud operations
	 */
	@Autowired
	private IForecastRepository iforecastrepository;

	@Autowired
	private ICategoryRepository icategoryrepository;

	@Autowired
	private IAparRepository iaparrepository;
	
	@Autowired
	private KafkaTemplate<String, Object> template;
	
//	@Autowired
//	private KafkaTemplate<String, String> template;

	/**
	 * Implementation method for getting all the available forecasts using forecast
	 * repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<Forecast> getAllForecasts() {
		logger.info("getting all the forecasts");
		if (iforecastrepository.findAll() == null) {
			throw new ForecastNotFoundException();
		} else {
//			template.send("test", "getting all the requested forecasts");
			return iforecastrepository.findAll();
		}
	}

	/**
	 * Implementation method for checking if a forecast exists for given id or not
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public boolean existsByID(UUID forecastID) {
		logger.info("checking for existence");
//		template.send("test", "getting the forecasts");
		return iforecastrepository.existsById(forecastID);
	}

	/**
	 * Implementation method for getting one forecast using forecast repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Forecast getForecastByID(UUID forecastID) {
		logger.info("getting forecast by id");
//		template.send("test", "get the forecast");
		return iforecastrepository.findById(forecastID).orElseThrow();
	}

	/**
	 * Implementation method for creating a new forecast using forecast repository
	 * 
	 * @return Created forecast object
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Forecast createNewForecast(Forecast forecast) {
		logger.info("creating a new forecast");
		List<String> list = iforecastrepository.findForecastName(forecast.getAccountID());
		if (list.contains(forecast.getForecastName())) {
			throw new ForecastAlreadyExists();
		} else {
			forecast.setStartDate(LocalDate.now());
			forecast.setEndDate(LocalDate.now().plusDays(forecast.getHorizonPeriod() * 7));
			forecast.setCreatedTime(LocalDate.now());
			forecast.setUpdatedTime(LocalDate.now());
			ByteBuffer startbuffer = StandardCharsets.UTF_8.encode(forecast.getStartDate().toString());
			ByteBuffer endbuffer = StandardCharsets.UTF_8.encode(forecast.getEndDate().toString());
			ByteBuffer createdbuffer = StandardCharsets.UTF_8.encode(forecast.getCreatedTime().toString());
			ByteBuffer updatedbuffer = StandardCharsets.UTF_8.encode(forecast.getUpdatedTime().toString());
			forecast.setStartDate(LocalDate.parse(StandardCharsets.UTF_8.decode(startbuffer).toString()));
			forecast.setEndDate(LocalDate.parse(StandardCharsets.UTF_8.decode(endbuffer).toString()));
			forecast.setCreatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(createdbuffer).toString()));
			forecast.setUpdatedTime(LocalDate.parse(StandardCharsets.UTF_8.decode(updatedbuffer).toString())); 
			try {
				String sendforecast = new ObjectMapper().writeValueAsString(forecast);
				template.send("test", forecast);
//				template.send("two", forecast);
				
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} 
//			return iforecastrepository.save(forecast);
		}
		return forecast;
	}

	
	/**
	 * Implementation method for updating an existing forecast using forecast
	 * repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Forecast updateForecastDetails(UUID forecastID, Forecast forecast) {
		logger.info("updating the forecast");
		ArrayList<String> list = iforecastrepository.findForecastName(forecast.getAccountID());
		if (list.contains(forecast.getForecastName())) {
			throw new ForecastAlreadyExists();
		} else {
			Forecast newforecast = iforecastrepository.findById(forecastID).orElseThrow();
			newforecast.setForecastName(forecast.getForecastName());
			newforecast.setForecastDescription(forecast.getForecastDescription());
			newforecast.setUpdatedTime(LocalDate.now());
			JsonNode sendforecast = new ObjectMapper().convertValue(newforecast, JsonNode.class);
			template.send("test", forecast);
			return forecast;
//			return iforecastrepository.save(newforecast);
		}
	}

	/**
	 * Implementation method for deleting an existing forecast using forecast
	 * repository
	 */
	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = {Exception.class}) public Map<String, Boolean>
	 * deleteForecast(UUID forecastID) { Forecast newforecast =
	 * iforecastrepository.findById(forecastID).orElseThrow();
	 * iforecastrepository.delete(newforecast); Map<String, Boolean> response = new
	 * HashMap<>(); response.put("deleted", Boolean.TRUE); return response; }
	 */

	/**
	 * Implementation method for deleting an existing public forecast based on
	 * visibility
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Map<String, Boolean> deleteForecast(String visibility, String forecastName)
			throws CategoryNotFoundException, TransactionNotFoundException, ForecastCannotBeDeleted {
		logger.info("deleting the forecast");
		if (visibility.contains("Public") || visibility.contains("public") || visibility.contains("PUBLIC")) {
			UUID forecastid = iforecastrepository.findForecastID(forecastName);
			Forecast forecast = iforecastrepository.findById(iforecastrepository.findForecastID(forecastName)).get();
			List<UUID> list = icategoryrepository.findCategoryForForecast(forecast);
			if (list != null) {
				for (UUID categoryuuid : list) {
//					 Map<String, Boolean> map = icategoryrepository.deleteCategory(categoryuuid);
					Category category = icategoryrepository.findById(categoryuuid).orElseThrow();
					iaparrepository.deleteAparForCategory(category);
					icategoryrepository.deleteById(categoryuuid);
				}
			}
//			icategoryrepository.deleteById(categoryuuid);
//			list.forEach(icategoryservice.deleteCategory(list.get(list.indexOf(list))));
//			template.send("test", "delete the forecast");
			iforecastrepository.deleteForecast(visibility, forecastName);
			Map<String, Boolean> response = new HashMap<>();
			response.put("deleted", Boolean.TRUE);
			return response;
		}
		{
			throw new ForecastCannotBeDeleted();
		}

	}

	/**
	 * Implementation method for getting forecast based on role and visibility
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<Forecast> findForecastsForRole(UUID roleID, String visibility) {
		logger.info("finding forecasts for role");
		if (iforecastrepository.findForecastsForRole(roleID, visibility) == null) {
			throw new ForecastNotFoundException();
		}
		{
//			template.send("test", "get by role");
			return iforecastrepository.findForecastsForRole(roleID, visibility);
		}
	}

	/**
	 * Implementation method for getting forecast names based on accountID
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public List<String> findForecastName(AccountDetails accountID) {
		logger.info("finding forecast by name");
//		template.send("test", "finding by name");
		return iforecastrepository.findForecastName(accountID);
	}

}
