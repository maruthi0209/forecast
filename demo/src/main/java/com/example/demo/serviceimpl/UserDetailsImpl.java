
package com.example.demo.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.exception.UserDetailsNotFoundException;
import com.example.demo.pojos.UserDetails;
import com.example.demo.repository.IUserDetailsRepository;
import com.example.demo.service.IUserDetailsService;

/**
 * This class implements the methods defined in the UserDetails service
 * interface by performing the related crud operations
 * 
 * @author schennapragada
 *
 */
@Transactional(rollbackFor = { Exception.class })
@Service
public class UserDetailsImpl implements IUserDetailsService {

	/**
	 * creating an instance of Userdetails repository interface for crud operations
	 */
	@Autowired
	private IUserDetailsRepository iuserdetailsrepository;
	
	@Autowired
	private KafkaTemplate<String, Object> kafkatemplate;

	/**
	 * Implementation method for getting all the available userdetails using
	 * UserDetails repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public Iterable<UserDetails> getAllUsers() {
		if (iuserdetailsrepository.findAll() == null) {
			throw new UserDetailsNotFoundException();
		} else {
		return iuserdetailsrepository.findAll();
	}
	}

	/**
	 * Implementation method for creating a new user using UserDetails repository
	 */
	@Override
	@Transactional(rollbackFor = { Exception.class })
	public UserDetails createNewUser(UserDetails userdetails) {
		return iuserdetailsrepository.save(userdetails);
//		kafkatemplate.send("test", userdetails);
//		return userdetails;
		
	}
}
