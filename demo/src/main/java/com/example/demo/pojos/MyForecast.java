package com.example.demo.pojos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *  Experimental entity for rough work
 * @author schennapragada
 *
 */
@Entity
@Table(name = "MyForecast")
public class MyForecast {
	@Id

	private Long fID;

	private String fname;

	private String uID;

	public MyForecast() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MyForecast(Long fID, String fname, String uID) {
		super();
		this.fID = fID;
		this.fname = fname;
		this.uID = uID;
	}

	public Long getfID() {
		return fID;
	}

	public void setfID(Long fID) {
		this.fID = fID;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getuID() {
		return uID;
	}

	public void setuID(String uID) {
		this.uID = uID;
	}

	@Override
	public String toString() {
		return "MyForecast [fID=" + fID + ", fname=" + fname + ", uID=" + uID + "]";
	}

}
