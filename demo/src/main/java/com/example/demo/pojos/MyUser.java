package com.example.demo.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *  Experimental entity for rough work
 * @author schennapragada
 *
 */
@Entity
@Table(name = "MyUser")
public class MyUser {
	@Id
	private String uID;

	public MyUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column(name = "uid", length = 5)
	public String getuID() {
		return uID;
	}

	public void setuID(String uID) {
		this.uID = uID;
	}

	@Override
	public String toString() {
		return "MyUser [uID=" + uID + "]";
	}

}
