package com.example.demo.pojos;

public class StandardReleaseResponse extends Exception {
	/**
	 * Defines the response code and response message for an exception occuring in
	 * the process
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Defines two fields response code and response message
	 */
	private String responsecode;
	private String responsemessage;

	public StandardReleaseResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StandardReleaseResponse(String responsecode, String responsemessage) {
		super();
		this.responsecode = responsecode;
		this.responsemessage = responsemessage;
	}

	/**
	 * Setters and getters for this entity
	 */

	public String getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(String string) {
		this.responsecode = string;
	}

	public String getResponsemessage() {
		return responsemessage;
	}

	public void setResponsemessage(String responsemessage) {
		this.responsemessage = responsemessage;
	}

	/**
	 *  To String method
	 */
	@Override
	public String toString() {
		return "StandardReleaseResponse [responsecode=" + responsecode + ", responsemessage=" + responsemessage + "]";
	}

}
