
package com.example.demo.service;

import com.example.demo.pojos.StandardReleaseResponse;
import com.example.demo.pojos.UserDetails;

/**
 * This interface is used as a service layer for operations between the
 * UserDetails controller and UserDetails repository
 * 
 * @author schennapragada
 *
 */
public interface IUserDetailsService {

	/**
	 * Method for returning all the available users details in the database
	 * 
	 * @return Iterable of type UserDetails
	 */
	public Iterable<UserDetails> getAllUsers();

	/**
	 * Method for creating a new user details entry into the database
	 * 
	 * @param userdetails
	 * @return object of type UserDetails
	 */
	public UserDetails createNewUser(UserDetails userdetails);
}
