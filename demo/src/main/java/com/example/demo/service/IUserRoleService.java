
package com.example.demo.service;

import com.example.demo.pojos.StandardReleaseResponse;
import com.example.demo.pojos.UserRole;

/**
 * This interface is used as a service layer for operations between the UserRole
 * controller and UserRole repository
 * 
 * @author schennapragada
 *
 */
public interface IUserRoleService {

	/**
	 * Method for returning all the available users roles in the database
	 * 
	 * @return Iterable of type UserRoles
	 */
	public Iterable<UserRole> getAllRoles();

	/**
	 * Method for creating a new user roles entry into the database
	 * 
	 * @return object of type UserRole
	 */
	public UserRole createNewRole(UserRole userrole);

}
