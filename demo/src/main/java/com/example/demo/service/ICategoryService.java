
package com.example.demo.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * This interface is used as a service layer for operations between the 
 *  category controller and category repository
 * @author schennapragada
 *
 */
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.StandardReleaseResponse;

public interface ICategoryService {

	/**
	 * Method for creating a new category of transaction
	 * 
	 * @param category
	 * @return object of type Category
	 */
	public Iterable<Category> getAllCategories();

	/**
	 * Method for getting all the available category of transaction from the
	 * database
	 * 
	 * @return Iterable of type Category
	 */
	public Category createNewCategory(Category category);

	/**
	 * Implementation method for finding the category ids based on forecast id
	 */
	List<UUID> findCategoryForForecast(Forecast forecastID);

	/**
	 * Implementation method for deleting categories by id
	 */
	Map<String, Boolean> deleteCategory(UUID uuid);

}
