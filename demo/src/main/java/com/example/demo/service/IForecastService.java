
package com.example.demo.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.Forecast;
import com.example.demo.pojos.StandardReleaseResponse;

/**
 * This interface is used as a service layer for operations between the Forecast
 * controller and forecast repository
 * 
 * @author schennapragada
 *
 */
public interface IForecastService {

	/**
	 * Method to get all the available forecast in the database
	 * 
	 * @return Iterable of type Forecast
	 */
	public Iterable<Forecast> getAllForecasts();

	/**
	 * Method to get a particular forecast detail from the database
	 * 
	 * @param forecastID
	 * @return Object of type Forecast
	 */
	public Forecast getForecastByID(UUID forecastID);

	/**
	 * Method for creating a new forecast
	 * 
	 * @param forecast object
	 * @return object of type forecast
	 */
	public Forecast createNewForecast(Forecast forecast);

	/**
	 * Method to update a particular forecast detail from the database based on
	 * forecase id
	 * 
	 * @param forecastID
	 * @param forecast
	 * @return Object of type Forecast
	 */
	public Forecast updateForecastDetails(UUID forecastID, Forecast forecast);

	/**
	 * Method for deleting a forecast from the database
	 * 
	 * @param forecastID
	 * @return Map of String and Boolean
	 */
//	public Map<String, Boolean> deleteForecast(UUID forecastID);

	/**
	 * Method for deleting an existing public forecast based on visibility
	 */
	public Map<String, Boolean> deleteForecast(String visibility, String forecastName);

	/**
	 * Method for getting forecast based on role and visibility
	 */
	public Iterable<Forecast> findForecastsForRole(UUID roleID, String visibility);

	/**
	 * Method to check if a forecast by this id exists or not
	 * 
	 * @param forecastID
	 * @return boolean
	 */
	public boolean existsByID(UUID forecastID);

	/**
	 * Method to get available forecast name for particular account id
	 * 
	 * @param accountID
	 * @return List of type string
	 */
	public List<String> findForecastName(AccountDetails accountID);



}
