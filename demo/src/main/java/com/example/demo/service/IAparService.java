
package com.example.demo.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

import com.example.demo.pojos.Apar;
import com.example.demo.pojos.Category;
import com.example.demo.pojos.Forecast;

/**
 * This interface is used as a service layer for operations between the Apar
 * controller and Apar repository
 * 
 * @author schennapragada
 *
 */
public interface IAparService {

	/**
	 * Method to get information about all the available transactions in the
	 * database for an account
	 * 
	 * @return Iterable of type Apar
	 */
	public Iterable<Apar> getAllTransactions();

	/**
	 * Method for creating a new transaction into the database
	 * 
	 * @param transaction
	 * @return object of type Apar
	 */
	public Apar createNewTrasaction(Apar transaction);
	

	/** 
	 *  Method for deleting a transaction based on id
	 * @return 
	 */
	public Map<String, Boolean> deleteTransaction(UUID uuid);

	/**
	 *  Method to return list of transaction IDs for a given category
	 * @param cateogry
	 * @return
	 */
	public Map<String, Boolean> deleteAparForCategory(Category cateogry);
}
