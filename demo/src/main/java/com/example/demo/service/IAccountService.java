
package com.example.demo.service;

import java.util.Optional;
import java.util.UUID;

import com.example.demo.pojos.AccountDetails;
import com.example.demo.pojos.StandardReleaseResponse;

/**
 * This interface is used as a service layer for operations between the Account
 * controller and Account repository
 * 
 * @author schennapragada
 *
 */
public interface IAccountService {

	/**
	 * Method to get information about all the available accounts in the database
	 * 
	 * @return Iterable of type AccountDetails
	 */
	public Iterable<AccountDetails> getAllAccounts();

	/**
	 * Method for creating a new account into the database
	 * 
	 * @param transaction
	 * @return object of type AccountDetails
	 */
	public AccountDetails createNewAccount(AccountDetails accountdetails);

	/**
	 *  Method for getting account details by id
	 * @param accountid
	 * @return object account details
	 */
//	public AccountDetails getDetailsByID(UUID accountID);

}
